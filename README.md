# Random Python stuff

## Fancy print:

Use the same as `print()`. Available keyword arguments are sep (defaults to space), end (defaults to newline) and max\_text\_width (defaults to 51).

Short prints:

```
╔═══ ∘◦ ☆ ◦∘ ═══╗
‖      Moi      ‖
╚═══ ∘◦ ❉ ◦∘ ═══╝
```

```
╔═══════════ ∘◦ ☆ ◦∘ ═══════════╗
‖ Moikka sdhf jshdjkf hsdjkhfa  ‖
╚═══════════ ∘◦ ❉ ◦∘ ═══════════╝
```

```
╔═══════════ ∘◦ ☆ ◦∘ ═══════════╗
‖ Moikka sdhf jshdjkf hsdjkhfad ‖
╚═══════════ ∘◦ ❉ ◦∘ ═══════════╝
```

Multiple arguments:

```python
fancy_print("Kahen luvun summa:", 47+82, "|", "Sanakirja:", {1:2, 3:4})
```

```
╔═════════════════════ ∘◦ ☆ ◦∘ ═════════════════════╗
‖ Kahen luvun summa: 129 | Sanakirja: {1: 2, 3: 4}  ‖
╚═════════════════════ ∘◦ ❉ ◦∘ ═════════════════════╝
```

Long text + word wrap demonstration:

```
╔══════════════════════ ∘◦ ☆ ◦∘ ══════════════════════╗
‖    Moikka tää printti on tosi pitkä ja tässä on     ‖
‖ super pitkä sana joka on hjshdfjdksfhjksdhfjsdkhfj- ‖
‖ dskhfjksdhfjkdshfjdkshfdksjhfsjdkfhjksdhfjkdshfjdk- ‖
‖ shfjsdkhfjskdhfjksdhfjksdhfjksdhfjkdshfjksdhfjkdsh- ‖
‖   fjksdhfkj mutta toivottavasti silti tulostuisi    ‖
‖  nätisti. Jos osaan koodata sen. Mutta eipä tässä   ‖
‖  mitään kirjottelen vaan jotain että voin testata   ‖
‖  tätä printtifunktiota. Nää pitäis mennä hienosti   ‖
‖                 omille riveilleen.                  ‖
╚══════════════════════ ∘◦ ❉ ◦∘ ══════════════════════╝
```

With newline characters (marked by ->), and max\_text\_width kwarg set to 60:

```
╔═══════════════════════════ ∘◦ ☆ ◦∘ ═══════════════════════════╗
‖      Moikka tää printti on tosi pitkä ja tässä on myös        ‖
‖               newlineja. Esim nyt tulee yks ->                ‖
‖  mutta toivottavasti silti tulostuisi nätisti. Tossa kaks ->  ‖
‖                                                               ‖
‖  Jos osaan koodata sen. Mutta eipä tässä mitään kirjottelen   ‖
‖                      vaan jotain että ->                      ‖
‖     voin testata tätä printtifunktiota. Nää pitäis mennä      ‖
‖                  hienosti omille riveilleen.                  ‖
╚═══════════════════════════ ∘◦ ❉ ◦∘ ═══════════════════════════╝
```

Fancy input:

```
╔═══ ∘◦ ☆ ◦∘ ═══╗
‖  Anna syöte:  ‖
╚═══ ∘◦ ❉ ◦∘ ═══╝

> jskdjk sfjdksf jlsdf

```
