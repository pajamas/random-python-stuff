from fancy_print import fancy_print

def fancy_input(prompt):
    fancy_print(prompt)
    i = input("> ")
    print()

    return i

if __name__ == "__main__":
    fancy_input("Anna syöte:")
