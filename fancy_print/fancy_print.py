def fancy_print(*args, **kwargs):
    # https://cutekaomoji.com/misc/borders/
    # ╔═════ ∘◦ ☆ ◦∘ ══════╗
    # ‖      tekstiä       ‖
    # ╚═════ ∘◦ ❉ ◦∘ ══════╝

    top_decoration = " ∘◦ ☆ ◦∘ "
    bottom_decoration = " ∘◦ ❉ ◦∘ "

    max_text_width = kwargs.get("max_text_width", 51)
    top_decoration = kwargs.get("top_decoration", " ∘◦ ☆ ◦∘ ")
    bottom_decoration = kwargs.get("bottom_decoration", " ∘◦ ❉ ◦∘ ")
    padding_amount = kwargs.get("padding_amount", 1)
    # separator
    sep = kwargs.get("sep", " ")
    # end character
    end = kwargs.get("end", "\n")
    
    text = sep.join(str(arg) for arg in args)
    rows = text.split("\n")
    max_line_width = len(max(rows, key=lambda row: len(row)))
    text_width = min(max_line_width, max_text_width)

    # borders
    # border_line is the long ═ part and its width depends on the length of the text
    # also there are two of them on each side of the decorative part
    border_line_top = max(3,(text_width + 1 + 2*padding_amount - len(top_decoration))//2) * "═"
    border_line_bottom = max(3,(text_width + 1 + 2*padding_amount - len(bottom_decoration))//2) * "═"
    border_top = "╔" + border_line_top + top_decoration + border_line_top + "╗"
    border_bottom = "╚" + border_line_bottom + bottom_decoration + border_line_bottom + ("" if len(border_top) % 2 == 1 and len(bottom_decoration) % 2 == 1 else "═") + "╝"
    border_sides = "‖"

    # make rows
    rows = []
    while len(text) > 0:
        text_width = min(len(text), max_text_width)
        if "\n" in text[:text_width]:
            text_row = text[:text.find("\n")]
            text_width = len(text_row)
            text = text[len(text_row) + 1:]
            rows.append(text_row.strip())
            continue
        text_row = ""
        while len(text) > 0:
            next_word = text if " " not in text else text[:text.find(" ") + 1]
            # if next word is really long and has to be wrapped
            if len(next_word) > max_text_width:
                next_word_part = next_word[:max_text_width-len(text_row)-1]
                text_row += text[:len(next_word_part)] + "-"
                text = text[len(next_word_part):]
                break
            # if next word fits in row
            elif len(text_row) + len(next_word) < max_text_width:
                text_row += next_word
                text = text[len(next_word):]
            # next word doesn't fit
            else:
                text_width = len(text_row)
                break
        rows.append(text_row.strip())

    # print everything
    print(border_top)

    for row in rows:
        # padding: at least 1 space, normally length of top border - length of text - 2
        # (-2 because of side borders)
        # divided by 2 (cause its on both sides of text)
        padding = max( padding_amount, ( len(border_top) - 2 - len(row) )//2 ) * " "
        padding_left = padding
        padding_right = padding
        if (len(row) % 2 == 0) and len(top_decoration) % 2 == 1 or (len(row) % 2 == 1) and len(top_decoration) % 2 == 0:
            padding_right += " "
        print(border_sides + padding_left + row + padding_right + border_sides)

    print(border_bottom)
    print(end=end)

if __name__ == "__main__":
    fancy_print("Moi")
    fancy_print("Moikka sdhf jshdjkf hsdjkhfa")
    fancy_print("Moikka sdhf jshdjkf hsdjkhfad")
    fancy_print("Moikka tää printti on tosi pitkä mutta toivottavasti silti tulostuisi nätisti. Jos osaan koodata sen. Mutta eipä tässä mitään kirjottelen vaan jotain että voin testata tätä printtifunktiota. Nää pitäis mennä hienosti omille riveilleen.")
    fancy_print("Moikka tää printti on tosi pitkä ja tässä on super pitkä sana joka on kanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakanakana mutta toivottavasti silti tulostuisi nätisti. Jos osaan koodata sen. Mutta eipä tässä mitään kirjottelen vaan jotain että voin testata tätä printtifunktiota. Nää pitäis mennä hienosti omille riveilleen.")
    fancy_print("Moikka tää printti on tosi pitkä ja tässä on myös newlineja. Esim nyt tulee yks ->\nmutta toivottavasti silti tulostuisi nätisti. Tossa kaks ->\n\nJos osaan koodata sen. Mutta eipä tässä mitään kirjottelen vaan jotain että ->\nvoin testata tätä printtifunktiota. Nää pitäis mennä hienosti omille riveilleen.", max_text_width=60)
    fancy_print("Kahen luvun summa:", 47+82, "|", "Sanakirja:", {1:2, 3:4})
    fancy_print("Teksti\njossa\non\ntosi\npaljon\nrivinvaihtoja\njoten\ntän\ntulostuksen\npitäis\nolla\naika\nohut", padding_amount=3, top_decoration=" x ", bottom_decoration=" .v.v. ")
