import pygame
import math


window_width = 1000
window_height = 800
window = pygame.display.set_mode((window_width, window_height))


fps = 60
clock = pygame.time.Clock()

running = True

phase = 0

back = False

original_radius_y = 10
original_radius_x = 20

height_factor = 400

while running:
    clock.tick(fps)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    radius_x = original_radius_x
    radius_y = original_radius_y
    center_x = 400 + 5*math.sin(phase / 20)
    center_y = 700 + 3*math.cos(phase / 20)

    window.fill((255,255,255))

    r = int(math.pi * original_radius_x * 2*height_factor)
    for i in range(r):
        s = math.cos((i + phase)/100)
        if s < 0:
            color = (255,0,0)
        else: color = (0,0,0)
        f = int(220*(1 - (s/2 + 0.5)))
        color = (f,f,f)
        x = int(center_x + 50*math.sin(i / 5000 + phase / 90) + math.sin((i + phase)/100)*radius_x)
        y = int(center_y + math.cos((i + phase)/100)*radius_y)
        window.set_at((x, y), color)
        center_y -= 0.01
        radius_x += 0.01 * (1.2*i/r)**2
        radius_y += 0.002 * i/r

    phase += 2
    #height_factor += 0.2

    pygame.display.update()
