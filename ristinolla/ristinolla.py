from time import sleep

def print_board(board):
    print("  "+"  A  B  C ")
    print("  "+"⚝──⭒─⭑─⭒──⚝")
    print("1 "+" "+" | ".join(board[0]))
    print("  "+"-----------")
    print("2 "+" "+" | ".join(board[1]))
    print("  "+"-----------")
    print("3 "+" "+" | ".join(board[2]))
    print("  "+"⚝──⭒─⭑─⭒──⚝")

def get_empty_board():
    return [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]

def get_move_coords(move):
    return (int(move[1])-1, "abc".find(move[0].lower()))

def check_move_validity(move):
    if len(move) != 2:
        return False
    if move[0].lower() not in "abc":
        return False
    if move[1] not in "123":
        return False
    return True

def check_move_availability(board, move):
    coords = get_move_coords(move)
    if board[coords[0]][coords[1]] != " ":
        return False
    return True

def check_win(board, turn):
    # rows
    for row in board:
        if row.count(turn) == 3:
            return True
    # columns
    columns = [[row[i] for row in board] for i in range(3)]
    for column in columns:
        if column.count(turn) == 3:
            return True
    # diagonals
    diagonal1 = [board[0][0], board[1][1], board[2][2]]
    if diagonal1.count(turn) == 3:
        return True
    diagonal2 = [board[2][0], board[1][1], board[0][2]]
    if diagonal2.count(turn) == 3:
        return True
    # no win
    return False

def check_tie(board):
    for row in board:
        for square in row:
            if square == " ":
                return False
    return True

def do_move(board, move, turn):
    coords = get_move_coords(move)
    board[coords[0]][coords[1]] = turn

def print_help():
    print()
    print("~ Ohjeet ~")
    print("Anna siirtosi muodossa <pystyrivi><rivi>, esim: B1")
    print("Se kumpi saa ekana kolme voittoa on koko pelin voittaja.")
    print()

def game():
    print(". ⋅ ˚̣- : ✧ Ristinolla ✧ : -˚̣⋅ .")
    sleep(0.5)
    print_help()

    board = get_empty_board()
    game_over = False
    turn = "X"
    points = {"X":0, "O":0}

    while not game_over:
        print_board(board)
        print(turn+":n vuoro.")
        while True:
            move = input("Anna siirtosi: ")
            if move == "help":
                print_help()
                continue
            if move == ":q":
                print("\nHeippa!")
                exit()
            if not check_move_validity(move):
                print("En tajunnut siirtoasi...")
            elif not check_move_availability(board, move):
                print("Ruutu on varattu!")
            else:
                break
        do_move(board, move, turn)
        if check_win(board, turn):
            points[turn] += 1
            print_board(board)
            print()
            print("«───── « ⋅ʚ♡ɞ⋅ » ─────»")
            print(f"{turn} voitti tämän erän!")
            print("Pisteet:")
            print(f"X: {points['X']}")
            print(f"O: {points['O']}")
            print("«───── « ⋅ʚ♡ɞ⋅ » ─────»")
            sleep(1.2)

            if points[turn] == 3:
                break

            board = get_empty_board()

        elif check_tie(board):
            print_board(board)
            print()
            print("«── « ⋅ʚ♡ɞ⋅ » ──»")
            print("Tasapeli!!")
            print("«── « ⋅ʚ♡ɞ⋅ » ──»")
            board = get_empty_board()
            sleep(0.9)
        if turn == "X":
            turn = "O"
        else:
            turn = "X"
        print()

    return points

while True:
    points = game()
    if points["X"] > points["O"]:
        winner = "X"
    else:
        winner = "O"

    print("\n...\n")
    print("«───── « ⋅ʚ♡ɞ⋅ » ─────»")
    print(f"Voittaja on: {winner}!")
    print("«───── « ⋅ʚ♡ɞ⋅ » ─────»")
    while True:
        print()
        play_again = input("Haluatko pelata uudelleen? (k/e))")
        if play_again != "k" and play_again != "e":
            print("En tajunnut...")
            continue
        break
    if play_again == "e":
        break

print()
print("Heippa!")
